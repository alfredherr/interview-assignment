package main

import (
	"fmt"
	"os"
	"strings"
	"testing"

	"github.com/tealeg/xlsx"
)

func main() {
	argsWithProg := os.Args
	excelFileName := os.Args[1:]

	fmt.Println(argsWithProg)
	fmt.Println(excelFileName)
	Run1(excelFileName)
}

func Run1(excelFileName []string) {
	Run(strings.Join(excelFileName, " "))
}
func Run(excelFileName string) {
	xlFile, err := xlsx.OpenFile(excelFileName)
	if err != nil {
		fmt.Printf("%s\n", "oops")

	}
	lines := 0
	for _, sheet := range xlFile.Sheets {
		for _, row := range sheet.Rows {
			for _, cell := range row.Cells {
				strings.Count(cell.String(), "")

				lines++
			}
		}
	}
	fmt.Printf("%d\n", lines)
}

func BenchmarkCalculate(b *testing.B) {

	Run("/Users/alfredherr/Projects/interview/demo.elixir/test-files/large-file.xlsx")

}
