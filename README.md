# Interview: Practical Assignment

## Problem Statement

Current solution for accepting xlsx files from clients is causing following issues:

1. Sales team reported some problems with the current approach:
   - It has been taking **much time to process** the entire file (sometimes containing a huge amount of data).
   - Sometimes **can’t import** some Card’s data due to simple **incompatibilities with data**, as value standards (eg. labels).
1. Infrastructure team also reported some problems with the current approach:
   - It has been using **too much** infrastructure resources such as **memory**, **CPU**, and **money**.
1. DBA team also reported some problems with the current approach:
   - It has been using **too much Database resources** and they’re facing **a lot of I/O** issues.

## design

In order to address the problems associated with memory, and CPU, and possibly I/O, we will stream the intake of the xlsx file on to disk directly, without having to load it all into memory first.

But because the file is in `xlsx`, and thus compressed/zipped, it must be decompressed before it can be processed. This causes some limitations in terms of how much cpu, IO and memory can be saved, regardless of technology being used. If the file was plain text it would be streamed right through without having to hit disk at all, and not 'understood' as a whole first.

In my tests of reading without streaming it, the memory usage is around the 2GB for a 1 million line xlsx file, even after decompressing to disk. I ran an alternative test using [java](../demo.java/java-project/) but it had the same issues. A quick test was done with Golang, and while the reading is fast, it still consumes a bit less than 3GB.

Results with streaming (after the file is decompressed to disk) are [here](./demo.elixir/benchmarks/output/results_comparison.html)

A second approach will be to use the OS to unzip the xlsx file, then use an XML reader library that knows how to consume the excel xml.

In order to minimize load on the DB, we need to hit the DB only to:

- Query to extract the validations/business rules that apply to the file (i.e. what the Admin has configured for the import)
- Bulk insert into database the valid cards, but only after the file has been successfully processed and it is known which data is good, and which is bad (see unit of work note below).

## language and frameworks

Elixir/OTP

Two options:

1. library `Xlsxir` - initial thoughts
1. combination of library `nimble_csv` and external xlsx to csv converter `xlsx2csv.rs` ([Rust](./xlsx2csv.rs/README.md))

## assumptions

We're assuming that:

- The **unit of work here is the file**. This is in terms of determining when processing is successful and before cards can be inserted into the DB. We don't want to have inserted cards into the DB and then an error processing the file halts execution before the 'bad data' file is created.

- I'm assuming we're not already streaming the files onto disk as they come in, but rather saving it all in memory, then onto disk, or worse doing it all in memory at once.

- That the xlsx parsing library we are using is faster than unzipping the xlsx file and processing the contents/xml manually/custom.

- That in fact, compressed/xlsx files can't be streamed unless they are decompressed. This needs more research on my end.

- That the existing infrastructure is already running elixir or erlang, in which case this new module can be added to replace the current one.

## points of validation

Things to measure:

### Number of Cards in File: ~100 (76K tiny-file.xlsx)

Results are [here](./demo.elixir/benchmarks/output/results_comparison.html)

| Time | CPU | IO |
|---|---|---|
| File acquisition | | |
| File Processing | | |
|Persistence | | |

### Number of Cards in File: ~1,000 (132K small-file.xlsx)

| Time | CPU | IO |
|---|---|---|
| File acquisition | | |
| File Processing | | |
|Persistence | | |

### Number of Cards in File: ~1,000,000 (38M large-file.xlsx)

| Time | CPU | IO |
|---|---|---|
| File acquisition | | |
| File Processing | | |
|Persistence | | |

## relevant tradeoffs

The problem statement does not specify the relationship between CPU, memory, IO or processing time. Between the 4, which is most important and to what degree is it most important? For example, if the key is being deployed in nano instances on AWS, where there little RAM, and less than a CPU, then perhaps processing time isn't the most important and we could optimize for ram/cpu, but it would certainly take longer to process.

Perhaps IO is where the cost is coming from, in which case, we could just load the file onto memory and process it all in RAM.

My approach here assumes more of the former, where we don't want large EC2 instances (and i'm assuming we're even on EC2 here).

## Potential alternatives

We could unzip the file manually, then process the raw xml sheets in a stream. This would help RAM and CPU, although not entirely IO.

Depending on the volume of this usage for this service, a hybrid deployment approach, where there are one or two standing larger machines which concentrate the processing of files on disk, being feed by a set of machines accepting the files from clients, but themselves only streaming the files to disk. More info is needed.

## deployment process

Elixir 1.9 natively supports building self-contained applications. This makes it straight forward to handle the service like a single package, container, etc, in a deployment pipeline.

CI/CD wasn't discussed for the purposes of this technical interview, however, it goes without saying that it should be in place.

## maintainability

Software maintainability

- The service is stateless
- Written in Elixir - a modular language  

Note that the external binary (Rust) component is an external dependency that must be available to the container running our service.

Changes

- When changes to existing rules occur
- When new validation rules are required

Operability

This service should be deployed as an internal component, or an additional component to the service currently serving HTTP.

This service requires temporary disk storage.

## Notes

```text
file =  "./test-files/large-file.xlsx"
m = Benchee.run(
     %{
         "test1 10" => fn -> InterviewDemo.BusinessLogic.get_processing_stream(file,"output1.txt",max_rows: 10) end,
         "test1 1_000" => fn -> InterviewDemo.BusinessLogic.get_processing_stream(file,"output2.txt",max_rows: 1_000) end,
         "test 100_000" => fn -> InterviewDemo.BusinessLogic.get_processing_stream(file,"output3.txt",max_rows: 100_000) end,

      },
      formatters: [
            Benchee.Formatters.HTML,
             Benchee.Formatters.Console
        ]
     )
```