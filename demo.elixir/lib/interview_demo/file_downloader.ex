defmodule InterviewDemo.FileDownloader do
  use Task

  def start_link(arg) do
    Task.start_link(__MODULE__, :stream_file, [arg])
  end

  def stream_file([%{url: url, gen_server: gen_server_pid, caller: caller_pid}]) do
    uu_id = "#{UUID.uuid1()}"
    file_name = "./temp/#{uu_id}.xlsx"
    {:ok, fd} = File.open(file_name, [:write, :binary])

    resp = HTTPoison.get!(url, %{}, stream_to: self(), async: :once)
    go(resp, fd, gen_server_pid, caller_pid, file_name, uu_id)
  end

  defp go(resp, fd, gen_server_pid, caller_pid, file_name, uu_id) do
    resp_id = resp.id

    receive do
      %HTTPoison.AsyncStatus{code: status_code, id: ^resp_id} ->
        IO.inspect(status_code)
        HTTPoison.stream_next(resp)
        go(resp, fd, gen_server_pid, caller_pid, file_name, uu_id)

      %HTTPoison.AsyncHeaders{headers: headers, id: ^resp_id} ->
        [{"Content-Type", x}, _ | _] = headers
        IO.inspect(x)
        HTTPoison.stream_next(resp)
        go(resp, fd, gen_server_pid, caller_pid, file_name, uu_id)

      %HTTPoison.AsyncChunk{chunk: chunk, id: ^resp_id} ->
        # IO.inspect(chunk)
        IO.binwrite(fd, chunk)
        HTTPoison.stream_next(resp)
        go(resp, fd, gen_server_pid, caller_pid, file_name, uu_id)

      %HTTPoison.AsyncEnd{id: ^resp_id} ->
        File.close(fd)
        GenServer.cast(gen_server_pid, {:finish_processing, file_name, uu_id, from: caller_pid})
    after
      2_000 ->
        File.close(fd)
        GenServer.cast(gen_server_pid, {:finish_processing, file_name, uu_id, from: caller_pid})
    end
  end
end
