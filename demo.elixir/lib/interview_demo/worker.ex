defmodule InterviewDemo.Worker do
  use GenServer
  alias InterviewDemo.FileDownloader
  alias InterviewDemo.BusinessLogic
  @convert "/Users/alfredherr/Projects/interview/xlsx2csv.rs/target/release/xlsx2csv"

  # Client
  def start_link(_) do
    GenServer.start_link(__MODULE__, %{})
  end

  def start_processing(pid, %{url: url, from: caller}) do
    GenServer.cast(pid, {:start_processing, url, caller})
  end

  # Server (callbacks)
  @impl true
  def init(_) do
    {:ok, []}
  end

  @impl true
  def handle_cast({:start_processing, url, caller}, state) do
    _task = FileDownloader.start_link([%{url: url, caller: caller, gen_server: self()}])

    {:noreply, state}
  end

  @impl true
  def handle_cast({:finish_processing, file_name, uuid, from: caller_pid}, state) do
    send(
      caller_pid,
      {:ok_msg,
       "File download complete. The file was saved to: #{file_name}. Starting processing..."}
    )

    #  test-files/large-file.xlsx -o ./temp/
    #   command_result =
    #    System.cmd(@converter, [file_name, "-o", "./temp/#{uuid}/"], into: IO.stream(:stdio, :line))

    #  IO.puts(inspect(command_result))
    # BusinessLogic.{:noreply, [%{file_name: file_name} | state]}
    {:noreply, state}
  end

  @impl true
  def handle_info(msg, state) do
    IO.puts("Got unexpected message #{inspect(msg)}")
    GenServer.stop(self(), {:unexpected_message, msg})
    {:noreply, state}
  end
end
