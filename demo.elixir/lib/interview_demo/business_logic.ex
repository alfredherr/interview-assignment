defmodule InterviewDemo.BusinessLogic do
  NimbleCSV.define(MyParser, separator: "\t", escape: "\"")
  alias Xlsxir, as: ExternalLibrary
  alias InterviewDemo.ColumnRule
  alias InterviewDemo.ValidationRules

  def apply_business_logic(input_file, output_file, options \\ []) do
    # table = :ets.new(:buckets_registry, [:set, :protected])
    header = %{counter: 0}

    ExternalLibrary.stream_list(input_file, 0, options)
    |> Stream.transform(
      header,
      fn data, accumulator ->
        %{counter: count} = accumulator
        {[data], %{accumulator | counter: count + 1}}
      end
    )
    |> Stream.map(&remove_nil_row(&1))
    |> Stream.into(File.stream!(output_file, [:write, :utf8]))
    |> Stream.run()
  end

  defp remove_nil_row([nil | _]) do
  end

  defp remove_nil_row(head) do
    head
  end

  defp validate(data, header) do
    IO.puts("Validating #{inspect(data, pretty: true)} #{inspect(header, pretty: true)}")
  end

  # First we take the header
  defp process_stream(data, 1, _) do
    # Get the header to know what fields are coming in
    ## TODO: Change this once we're getting configuration data from the DB

    # Turn it into a map of rules we need to run on this file
    rules_to_execute = map_header_to_rules(data)

    Enum.zip(data, rules_to_execute)
    |> Enum.map(fn {data, rules} ->
      IO.puts("#{inspect(data, pretty: true)} - #{inspect(rules, pretty: true)}")
    end)

    Enum.zip(data, rules_to_execute)
  end

  defp process_stream(data, row_index, acc) do
    [{_, rules} | _rest] = acc

    Enum.zip(data, rules)
    |> Enum.map(fn {d, r} -> IO.puts("#{d} - #{r}") end)

    cond do
      row_index > 1 ->
        [Enum.zip(data, rules) | acc]

      true ->
        [Enum.zip(data, rules)]
    end
  end

  defp map_header_to_rules(header) do
    rules = [{0, %ColumnRule{}}]

    # We can throw the first one away since it's our initial empty one
    # since i want to avoid having to create a new accumulator function to handle the empty list
    [_ | mapped_columns] = header |> Enum.reduce(rules, &column_accumulator/2) |> Enum.reverse()

    mapped_columns
  end

  defp column_accumulator(nil, [{column_number, _r} | _rest] = acc) do
    column = column_number + 1
    # Column has nil but still need to note the column position
    [{column, %ColumnRule{column_num: column}} | acc]
  end

  defp column_accumulator(row, [{column_number, r} | _rest] = acc) do
    field = String.downcase(row, :default)
    column = column_number + 1

    cond do
      String.contains?(field, "phone") ->
        rule = %ValidationRules{
          phone: true,
          short_text: r.use_rule.short_text,
          email: r.use_rule.email,
          long_text: r.use_rule.long_text
        }

        acc = [
          {column, %ColumnRule{column_name: field, column_num: column, use_rule: rule}} | acc
        ]

        acc

      String.contains?(field, "short") ->
        rule = %ValidationRules{
          phone: r.use_rule.phone,
          short_text: true,
          email: r.use_rule.email,
          long_text: r.use_rule.long_text
        }

        acc = [
          {column, %ColumnRule{column_name: field, column_num: column, use_rule: rule}} | acc
        ]

        acc

      String.contains?(field, "email") ->
        rule = %ValidationRules{
          phone: r.use_rule.phone,
          short_text: r.use_rule.short_text,
          email: true,
          long_text: r.use_rule.long_text
        }

        acc = [
          {column, %ColumnRule{column_name: field, column_num: column, use_rule: rule}} | acc
        ]

        acc

      String.contains?(field, "long") ->
        rule = %ValidationRules{
          phone: r.use_rule.phone,
          short_text: r.use_rule.short_text,
          email: r.use_rule.email,
          long_text: true
        }

        acc = [
          {column, %ColumnRule{column_name: field, column_num: column, use_rule: rule}} | acc
        ]

        acc

      true ->
        acc
    end
  end

  defp email(%ValidationRules{email: true}) do
    # Email - The email formatted as Plain Text.
  end

  defp phone(%ValidationRules{phone: true}) do
    # Phone - Numbers in the format of phone numbers of the country you want. Brazil: + 55 41 1234-5678.
    # Phone - (use a single quotation mark before the plus sign)
  end

  defp short_text(%ValidationRules{short_text: true}) do
    # Short/Long Text - The text formatted as Plain Text.
  end

  defp long_text(%ValidationRules{long_text: true}) do
    # Short/Long Text - The text formatted as Plain Text.
  end
end
