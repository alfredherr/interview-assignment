defmodule InterviewDemo.Application do
  use Application

  def start(_type, args) do
    children = [
      {InterviewDemo.Worker, args}
    ]

    opts = [strategy: :one_for_one, name: InterviewDemo.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
