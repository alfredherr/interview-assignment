defmodule InterviewDemo.ColumnRule do
  alias InterviewDemo.ValidationRules

  defstruct column_name: "", column_num: 0, use_rule: %ValidationRules{}
end
