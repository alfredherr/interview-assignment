defmodule InterviewDemo.ValidationRules do
  ######################
  # All Validation rules
  ######################
  # Assignee - The users' emails or the users' names separated by commas and formatted as Plain Text.
  # Attachment - Not supported.
  # CNPJ - Fourteen numbers following the pattern 000.000.00/0000-00 formatted as Plain Text.
  # Connection Field - The card's ID or record's ID formatted as Plain Text.
  # CPF - Eleven numbers following the pattern 000.000.000-00 formatted as Plain Text.
  # Currency - The value formatted as Currency.
  # Date - The value formatted as a date.
  # Date Time and Due Date - The value formatted as a Date time.
  # Email - The email formatted as Plain Text.
  # ID - Not supported.
  # Label Select - The text of the labels' names separated by commas and formatted as Plain Text.
  # Number - The number formatted as Plain Text.
  # Phone - Numbers in the format of phone numbers of the country you want. Brazil: + 55 41 1234-5678.
  # Phone - (use a single quotation mark before the plus sign)
  # Select - The text of the selected option formatted as Plain Text.
  # Short/Long Text - The text formatted as Plain Text.
  # Statement - Not supported.
  # Time - The value formatted as Time.
  # Vertical/Horizontal Checklist - The text of the checklist options separated by commas and formatted as Plain Text.
  # Vertical/Horizontal Radio - The text of the selected option formatted as Plain Text.

  defstruct phone: false, short_text: false, email: false, long_text: false
end
