defmodule InterviewDemo.MixProject do
  use Mix.Project

  def project do
    [
      app: :interview_demo,
      version: "0.1.0",
      elixir: "~> 1.9",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {InterviewDemo.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:httpoison, "~> 1.6"},
      {:benchee, "~> 1.0", only: :dev},
      {:uuid, "~> 1.1"},
      {:xlsxir, "~> 1.6.4"},
      {:benchee_html, "~> 1.0", only: :dev},
      {:nimble_csv, "~> 0.6"}
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"}
    ]
  end
end
