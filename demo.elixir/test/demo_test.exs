defmodule DemoTest do
  use ExUnit.Case
  doctest InterviewDemo.Application

  alias Xlsxir, as: ExternalLibrary

  setup_all do
    File.rm_rf!("./temp/")
    File.mkdir!("./temp/")
  end

  def tiny_file() do
    "./test-files/tiny-file.xlsx"
  end

  def small_file() do
    "./test-files/small-file.xlsx"
  end

  def large_file() do
    "./test-files/large-file.xlsx"
  end

  def local_path() do
    "./test-files/tiny-file.xlsx"
  end

  def remote_path() do
    # Random place on the internet
    # url = "https://file-examples.com/wp-content/uploads/2017/02/file_example_XLSX_5000.xlsx"

    # From Dropbox
    # url = "https://www.dropbox.com/s/y2lk44e138fbv2g/test_source_file.xlsx?dl=0"

    # From Google Sheets
    "https://docs.google.com/spreadsheets/d/1gSCiWsMUjkoWlpFQ1EKnmrzuQMl7iRao/export?format=xlsx"
  end

  test "business logic test" do
    file = tiny_file()
    InterviewDemo.BusinessLogic.get_processing_stream(file)
  end

  @tag :skip
  test "get headers" do
    {:ok, table} = ExternalLibrary.peek(local_path(), 0, 1)
    header = Xlsxir.get_map(table)
    IO.puts(inspect(header, pretty: true))
  end

  @tag :skip
  test "produces a stream" do
    ExternalLibrary.stream_list(local_path(), 0)
    |> Enum.map(&IO.inspect(&1))

    assert true
  end

  @tag :skip
  test "remote files test" do
    url = remote_path()

    {:ok, pid} = InterviewDemo.Worker.start_link("")

    InterviewDemo.Worker.start_processing(pid, %{url: url, from: self()})

    result =
      receive do
        {:ok_msg, msg} ->
          IO.puts(msg)
          {:ok}

        {:ok, msg} ->
          IO.puts(msg)
          {:ok}

        other ->
          IO.puts("#{inspect(other)}")
          {:error}
      after
        100_000 ->
          IO.puts("Done")
          {:timed_out}
      end

    assert {:ok} == result
  end

  @tag :skip
  test "test naive" do
    test_naive(local_path())
    assert true == true
  end

  @tag :skip
  test "test multi extract" do
    test_multi_extract(local_path())
    assert true == true
  end

  @tag :skip
  test "test peek" do
    test_peek(local_path())
    assert true == true
  end

  #####
  ##### Helpers
  #####

  def test_naive(file) do
    {:ok, _contents} = File.read(file)
    IO.puts("Done test_naive")
  end

  def test_multi_extract(file) do
    Xlsxir.multi_extract(file)
    IO.puts("Done test_multi_extract")
  end

  def test_peek(file) do
    {:ok, _table} = ExternalLibrary.peek(file, 0, 1)
    IO.puts("Done test_peek")
  end
end
