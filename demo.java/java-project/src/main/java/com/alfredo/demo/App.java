package com.alfredo.demo;

import com.monitorjbl.xlsx.StreamingReader;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;


import org.apache.log4j.Logger;
/**
 * Hello world!
 */
public class App {
    final static Logger logger = Logger.getLogger(App.class);

    public static void main(String[] args) {
        System.out.println("Hello World!");
        if (args.length < 1) {
            System.err.println("Please provide a file path!");
            System.exit(0);
        }
        run(args[0]);
//        try {
//            System.in.read();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

    }

    public static void run(String path) {
        try (
                InputStream is = new FileInputStream(new File(path));
                Workbook workbook = StreamingReader.builder()
                        .rowCacheSize(5)
                        .bufferSize(512)
                        .open(is)) {

            Path output_path = Paths.get("./output.txt");

            if(logger.isDebugEnabled()){
                logger.debug("Output file: " + output_path);
            }


            try (BufferedWriter writer = Files.newBufferedWriter(output_path)) {

                for (Sheet sheet : workbook) {
                    if(logger.isInfoEnabled()){
                        logger.info("The sheet name is " + sheet.getSheetName());
                    }

                    System.out.println(sheet.getSheetName());
                    for (Row r : sheet) {
                        for (Cell c : r) {

                            writer.write(c.getStringCellValue() + "|");
                        }
                        writer.write( "\n");
                    }

                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
